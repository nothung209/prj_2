import React, { useEffect } from 'react';
import { useState } from 'react';
import {useDispatch, useSelector} from'react-redux'
import { signin } from '../../actions/user.action';
import LoadingBox from '../LoadingBox.js'
import MessageBox from '../MessageBox.js'
import './form.css';

function LogIn(props){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const redirect= '/'
    const userSignin = useSelector((state) => state.userSignin)
    const {userInfo, loading, error} = userSignin;

    const dispatch = useDispatch(); 
    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(signin(email, password))
    };

    useEffect(() => {
        if(userInfo) {
            props.history.push(redirect);
        }
    },[props.history, redirect, userInfo])

    return(props.trigger) ? (
        <div className='Log_In'>
        <form className='inner' onSubmit={submitHandler}>
        <button onClick={()=>props.setTrigger(false)} className="close">&times;</button>
            <div className='container'>
            {loading && <LoadingBox></LoadingBox>}
            {error &&  <MessageBox vaiant = 'danger'>Sai ten dang nhap hoac mat khau</MessageBox>}
                <label for='uname' className='uname' htmlFor="email"><b>Tên Đăng Nhập</b></label> <br/>
                <input type='email' placeholder='Nhập Tên đăng nhập' name='uname' required onChange={(e) => setEmail(e.target.value)}></input>
                <br/>
                <label for="psw" className='psw' htmlFor="password"><b>Mật khẩu</b></label> <br/>
                <input type="password" placeholder="Nhập mật khẩu" name="psw" required onChange={(e) => setPassword(e.target.value)}></input> <br/>
                <button type="submit" className='btnl'>Đăng Nhập</button>
            </div>
        </form>
    </div>
    ) : '';
}
export default LogIn;