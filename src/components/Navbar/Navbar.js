import React from 'react';
import { MenuItems } from "./MenuItems";
import './Navbar.css';
import LogIn from '../Dropdown-log-in/form';
import {useState} from 'react';
import Cart from '../shoppingcart/cart';
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { signout } from '../../actions/user.action';



function Navbar(props) {
  const userSignin = useSelector((state) => state.userSignin)
  const {userInfo} = userSignin;
  const {onAdd, cartItems,onRemove} = props;
  const [buttonPoppup,setButtonPoppup] = useState(false);
  const [buttonPoppup1,setButtonPoppup1] = useState(false);
  const dispatch = useDispatch();
  const signoutHandler = () => {
      dispatch(signout())
  }
  
        return(
            <nav className='NavbarItems'>
                <Link to='/'><h1 className='navbar-logo'>JUNO<i className='fab fa-react'></i></h1></Link>
                <div className='menu-icon' >
                    
                </div>
                <ul className='nav-menu'>
                  {MenuItems.map((item, index ) => {
                      return (
                        <li key={index}>
                          <a className={item.cName} href={item.url}>{item.title}</a>
                        </li>
                      )
                  })}  
                </ul>
                   {
                     userInfo ? (
                      <div className='dropdown'>
                        <Link to="#">{userInfo.name} <i className='fa fa-caret-down'></i> </Link>
                        <ul className='dropdown-content'> 
                          <Link to='/' onClick={signoutHandler}>Dang xuat</Link>
                        </ul>
                      </div>
                     ) : (
                       <div>
                        <button className='dangnhap' onClick={() => setButtonPoppup(true)}>Đăng nhập</button>
                        <LogIn trigger={buttonPoppup} setTrigger={setButtonPoppup}></LogIn>
                      </div>
                     )
                   }
                   <button className='cartbtn' onClick={() => setButtonPoppup1(true)}><i className="fas fa-shopping-cart"></i>  ({cartItems.length})</button>
                   <Cart trigger={buttonPoppup1} setTrigger={setButtonPoppup1} onAdd={onAdd} onRemove={onRemove} cartItems={cartItems}></Cart>
            </nav>
        )
}
export default Navbar;