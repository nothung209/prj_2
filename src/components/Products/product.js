import React, { useEffect}  from 'react';
import {useDispatch, useSelector} from 'react-redux'
import './product.css';
import LoadingBox from '../LoadingBox';
import MessageBox from '../MessageBox';
import { listProducts } from '../../actions/product.action';
import { Link } from 'react-router-dom';





function Product(props) {
    const { onAdd } = props;
    const dispatch = useDispatch()
    const productList = useSelector(state => state.productList)
    const {loading, error, products} = productList
    useEffect(() => {
        dispatch(listProducts())
    }, [dispatch])
    
        return (
            <div>
                {loading ? (
                    <LoadingBox></LoadingBox>
                ) : error ? (
                    <MessageBox variant="danger">{error}</MessageBox>
                ) : (
                <div className='row'>
                    {products.map((product, index) => {
                        return (
                        <div className='s' key={index}>
                            <div className ='col-sm-3'>
                                <div className={product._id}>
                                    <img className="card-img-bottom" src={product.img1} alt="Card"></img>

                                    <div className="card-body">
                                        <Link to={`/product/${product._id}`} className='anchor'><h4 className="card-title">{product.title}</h4></Link>
                                        <p className="card-text">{product.price}</p>
                                    </div>
                                </div>
                            </div>

                            <div className='contain'>
                                <button className='btn' onClick={() => onAdd(product)}>Cho vào giỏ</button>
                            </div>
                        </div>
                        )
                    })}
                </div>
                )}
            </div>
        )
}


export default Product;