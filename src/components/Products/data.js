const data ={ 
    products: [
    {
        title : 'Giày Búp Bê Mũi Nhọn Khóa Trang Trí',
        img1: "https://product.hstatic.net/1000003969/product/xam_bb03062_1_0a2b3bf7aae74a2e86bec5ecc0d53aba_master.jpg",
        imgalt:"https://juno.vn/products/gia-y-cao-go-t-pump-khoa-trang-tri",
        price: '300.000đ',
        id:'1'
    },
    {
        title : 'Giày Sandal Mũi Vuông Gót Vuông',
        img1: 'https://product.hstatic.net/1000003969/product/xanh-tim_sd09083_1_370e5290f82f4b6f9a1b3ad08ed9d65c_master.jpg',
        price: '300.000đ',
        id:'2'
    },
    {
        title : 'Giày Cao Gót Pump Khóa Trang Trí',
        img1: 'https://product.hstatic.net/1000003969/product/xam_cg09124_1_cac424b1034a46d2a8974e8e36d7334f_master.jpg',
        price: '300.000đ',
        id:'3'
    },
    {
        title : 'Giày Cao Gót Quai Ngang Khóa Trang Trí',
        img1: 'https://product.hstatic.net/1000003969/product/cam_cg05101_1_a4f30a0910304fa0abd3d4db426d96f9_master.jpg',
        price: '300.000đ',
        id:'4'
    },
    {
        title : 'Giày Sandal Xuồng Quai Mũi Ngang Phối Pvc',
        img1: 'https://product.hstatic.net/1000003969/product/xanh-tim_sd07054_1_64ebedc4285c4f5ca1e08f01409ddd12_master.jpg',
        price: '350.000đ',
        id:'5'
    },
    {
        title : 'Juno Active Starter 6',
        img1: 'https://product.hstatic.net/1000003969/product/hong_tt05002_1_15935e51173e447b892c0381dd501aed_master.jpg',
        price: '600.000đ',
        id:'6'
    },
    {
        title : 'Giày Sandal Gót Thanh Phối Si Dập Ly',
        img1: 'https://product.hstatic.net/1000003969/product/xanh_sd11011_1_ab3f46b5da2e49bcb871f33b734f75fe_master.jpg',
        price: '400.000đ',
        id:'7'
    },
    {
        title : 'Giày Cao Gót Phối Hai Tone Màu',
        img1: 'https://product.hstatic.net/1000003969/product/kem_cg05100_1_0847bd62b7bf45a2a17611552c542d1c_master.jpg',
        price: '300.000đ',
        id:'8'
    },
    ] 
}  
export default data;