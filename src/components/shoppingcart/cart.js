import './cart.css';
import React from 'react';
import {Link} from 'react-router-dom';

function Cart(props){

    const { cartItems, onAdd, onRemove} = props;


    return (props.trigger) ? (
    <div className='side'>
        <div className = 'contain1'>
            <button onClick={()=>props.setTrigger(false)} className="close">&times;</button>
            <h3 className='title'>Giỏ hàng</h3>
            <div className='giohang'>
            {cartItems.map((item, index) => (
                <div key={index}>
                    <div className='it'>
                        <img className="anh" src={item.img1} alt="Card"></img> 
                        {item.title}
                        <p className='gia'>{item.price}</p>
                        <div className="col-2">
                            <button onClick={() => onRemove(item)} className="remove">
                                -
                            </button>{item.qty}
                            <button onClick={() => onAdd(item)} className="add">
                                +
                            </button>
                        </div>
                    </div>
                </div>
            ))}
            </div>
            <Link to='/thanhtoan'><button className='thanhtoan' onClick={()=>props.setTrigger(false)}>Checkout</button></Link>
        </div>
    </div>
    ) : '';
}

export default Cart;