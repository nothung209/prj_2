import { useState } from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import './App.css';
import HomeScreen from './screen/HomeScreen';
import ProductScreen from './screen/ProductScreen';
import CartScreen from './screen/CartScreen'
import Navbar from './components/Navbar/Navbar';
import { Switch } from 'react-router-dom/cjs/react-router-dom.min';


function App(props) {
  const [cartItems, setCartItems] = useState([]);
  const onAdd = (product) => {
      const exist = cartItems.find((x)=>x._id===product._id);
      if(exist){
          setCartItems(
          cartItems.map((x)=>x._id===product._id ? {...exist,qty: exist.qty + 1} : x)
          )
          
       
      } else{
      setCartItems([...cartItems, { ...product,qty :1}])
   
      }
  }
  const onRemove = (product) => {
      const exist = cartItems.find((x) => x._id === product._id);
      if (exist.qty === 1) {
        setCartItems(cartItems.filter((x) => x._id !== product._id));
        
      } else {
        setCartItems(
          cartItems.map((x) =>
            x._d === product._id ? { ...exist, qty: exist.qty - 1} : x
          )
        ) 
      }
  };

  const onAdd1 = (product,num) => {
    const exist = cartItems.find((x)=>x._id===product._id);
    if(exist){
        setCartItems(
        cartItems.map((x)=>x._id===product._id ? {...exist,qty: exist.qty + num} : x)
        )
        
     
    } else{
    setCartItems([...cartItems, { ...product,qty : num}])
 
    }
  }
  

    
 
  return (
    <BrowserRouter>
      <div className='App'> 
        <Switch>
          <Route path='/' render={(props)=> <HomeScreen {...props} onAdd={onAdd}></HomeScreen>} exact></Route>
          <Route path='/product/:id' render={(props)=><ProductScreen {...props} onAddtoCart={onAdd1}></ProductScreen>}></Route>
          <Route path='/thanhtoan' render={(props) =>  <CartScreen {...props} cartItems={cartItems}></CartScreen>}></Route>
        </Switch>
        <Navbar cartItems={cartItems} onAdd={onAdd} onRemove={onRemove}></Navbar>
      </div>
    </BrowserRouter>
  );
}

export default App;
