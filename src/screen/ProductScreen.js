import { useState,useEffect } from "react";
import './ProductScreen.css'
import { useDispatch, useSelector } from "react-redux";
import LoadingBox from '../components/LoadingBox'
import MessageBox from '../components/MessageBox'
import { detailsProduct } from "../actions/product.action";


export default function ProductScreen(props){
    const dispatch = useDispatch()
    const productId = props.match.params.id
    const productDetails = useSelector(state => state.productDetails)
    const {loading,error,product} = productDetails
    useEffect(() => {
        dispatch(detailsProduct(productId))
    }, [dispatch, productId])
    
    const {onAddtoCart} = props
    const [qty,setQty] = useState(0) ;
    const Add= (x) => {
        setQty(qty+1)
        return qty;
    }
    
    const Remove = (x) => {
        if(qty >= 1) setQty(qty-1)
        return qty;
    }

    if(!product){
        return <div>Product not found!!</div>
    }
    return (
        <div>
            {loading ? (
                    <LoadingBox></LoadingBox>
                ) : error ? (
                    <MessageBox variant="danger">{error}</MessageBox>
                ) : (
                    <div className='row'>
                        <div className='col-2'>
                            <img className='large' src={product.img1} alt={product.title}></img>
                        </div>
                        <div className='col-1'>
                            <ul>
                                <li className='nameP'>{product.title}</li>
                                <li className='priceP'>
                                    Price: {product.price}
                                    <br/>
                                    QTY
                                    <button className='remove' onClick={() => Remove(product)}>-</button>
                                    {qty}    
                                    <button className='add' onClick={() => Add(product)}>+</button> <br/>
                                    <button className="AddtoCart" onClick={() => onAddtoCart(product,qty)}>Them vao gio</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                )}
        </div>
    )
}