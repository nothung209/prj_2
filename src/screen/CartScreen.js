import './CartScreen.css';

export default function CartScreen(props) {
    const {cartItems} = props;
    const itemsPrice = cartItems.reduce((a, c) => a + parseInt(c.qty) * parseInt(c.price), 0);
    return(
        <div className='cartscreen col-2'>
             {cartItems.map((item, index) => (
                <div key={index}>
                    <div className='checkout-items'>
                        <img className="anh" src={item.img1} alt="Card"></img> 
                        {item.title}
                        <p className='gia'>{item.price}</p>
                        <div className="col-2">
                            {item.qty}
                        </div>
                    </div>
                </div>
            ))}
            <strong className='totalPrice'>
                {cartItems.length > 0 ? itemsPrice : 'Empty'}
            </strong>
        </div>
    )
}